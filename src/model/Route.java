package model;

import Util.DateUtil;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Geerard
 */
public class Route {

    private ArrayList<Entry> entries;
    private String checkSum;

    public Route(File logFile) throws FileNotFoundException {
        Scanner sc = new Scanner(logFile);
        checkSum = sc.nextLine();
        entries = new ArrayList();
        Entry previousEntry = null;
        while (sc.hasNextLine()) {
            Entry newEntry = new Entry(sc.nextLine(), sc.nextLine(), previousEntry);
            entries.add(newEntry);
            previousEntry = newEntry;
        }
    }

    public ObservableList<Entry> getEntries() {
        return FXCollections.observableArrayList(entries);
    }

    public ObservableList<Entry> getActiveEntries() {
        ObservableList list = FXCollections.observableArrayList();
        for (Entry e : entries) {
            if (e.isActive()) {
                list.add(e);
            }
        }
        return list;
    }

    public List<String> getallDates() {
        List<String> dates = new ArrayList();
        for (Entry e : entries) {
            dates.add(DateUtil.format(e.getTime()));
        }
        return dates;

    }

    public float getTotalActiveDistance() {
        float distance = 0;
        for (Entry e : getActiveEntries()) {
            distance += e.getDistanceC();
        }
        return distance;
    }

    @Deprecated
    public float getTotalDistance() {
        float distance = 0;
        for (Entry e : getEntries()) {
            distance += e.getDistanceC();
        }
        return distance;
    }
}
