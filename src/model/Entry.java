package model;

import Util.DateUtil;
import java.util.Calendar;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Geerard
 */
public class Entry {
    //access voor tableview columns

    private SimpleFloatProperty longitudeC = new SimpleFloatProperty();
    private SimpleFloatProperty latitudeC = new SimpleFloatProperty();
    private SimpleFloatProperty speedC = new SimpleFloatProperty();
    private SimpleFloatProperty distanceC = new SimpleFloatProperty();
    private SimpleStringProperty dirC = new SimpleStringProperty();
    private boolean active;
    private String latitudeDir;
    private String longitudeDir;
    private Entry previousEntry;
    private Entry nextEntry;
    private Calendar time;

    public Entry(String lineGPGGA, String lineGPRMC, Entry previous) {
        String[] GPGGAchunks = lineGPGGA.split(",");
        String[] GPRMCchunks = lineGPRMC.split(",");

        String time = GPGGAchunks[1];
        String date = GPRMCchunks[9];
        this.time = Calendar.getInstance();
        this.time.set(Calendar.MONTH, Integer.parseInt(date.substring(2, 4)));
        this.time.set(Calendar.YEAR, Integer.parseInt(date.substring(4, 6)) + 2000);
        this.time.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date.substring(0, 2)));
        this.time.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time.substring(0, 2)));
        this.time.set(Calendar.MINUTE, Integer.parseInt(time.substring(2, 4)));
        this.time.set(Calendar.SECOND, Integer.parseInt(time.substring(4, 6)));
        latitudeC.set(getGradenNotatie(GPGGAchunks[2]));
        latitudeDir = GPGGAchunks[3];
        longitudeC.set(getGradenNotatie(GPGGAchunks[4]));
        longitudeDir = GPGGAchunks[5];
        dirC.set(latitudeDir + longitudeDir);
        previousEntry = previous;
        if (previousEntry != null) {
            previousEntry.setNextEntry(this);
        }
        speedC.set(0.0f);
        distanceC.set(0.0f);

    }

    public Calendar getTime() {
        return time;
    }

    public String getTimeC() {
        return DateUtil.format(time);
    }

    public float getLongitudeC() {
        return longitudeC.get();
    }

    public float getLatitudeC() {
        return latitudeC.get();
    }

    public float getSpeedC() {
        return speedC.get();
    }

    public float getDistanceC() {
        return distanceC.get();
    }

    public String getDirC() {
        return dirC.get();
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean bool) {
        active = bool;
        this.updateDistance();
        if (nextEntry != null) {
            nextEntry.updateDistance();
        }
    }

    //NMEA in formaat XXYY.YYYY
    //of XXXYY.YYYY
    public float getGradenNotatie(String NMEA) {
        if (NMEA.charAt(4) == '.') {
            float XX = Integer.parseInt(NMEA.substring(0, 2));
            float YYYYYY = Integer.parseInt(NMEA.substring(2, 4) + NMEA.substring(5, 9));
            return XX + (YYYYYY / 600000);
        } else {
            float XX = Integer.parseInt(NMEA.substring(0, 3));
            float YYYYYY = Integer.parseInt(NMEA.substring(3, 5) + NMEA.substring(6, 10));
            return XX + (YYYYYY / 600000);
        }
    }

    private float getDistance(Entry e2) {
        if (e2 == null) {
            return 0.0f;
        }
        final int R = 6371; // Radious of the earth
        float lat1 = this.latitudeC.get();
        float lon1 = this.longitudeC.get();
        float lat2 = e2.latitudeC.get();
        float lon2 = e2.longitudeC.get();
        float latDistance = toRad(lat2 - lat1);
        float lonDistance = toRad(lon2 - lon1);
        float a = (float) (Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(toRad(lat1)) * Math.cos(toRad(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2));
        float c = (float) (2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)));
        float distance = R * c * 1000; //in meter
        return distance;
        //System.out.println("The distance between two lat and long is::" + distance);
    }

    private static float toRad(float value) {
        return (float) (value * Math.PI / 180.0);
    }

    private void setNextEntry(Entry next) {
        this.nextEntry = next;
    }

    private void updateDistance() {
        if (previousEntry == null) {
            return;
        }
        if (previousEntry.isActive()) {
            distanceC.set(getDistance(previousEntry));
            float millisPassed = this.time.getTimeInMillis() - previousEntry.time.getTimeInMillis();
            speedC.set((distanceC.get() / millisPassed) * 3600);
        } else {
            distanceC.set(0.0f);
            speedC.set(0.0f);
        }

    }
}
