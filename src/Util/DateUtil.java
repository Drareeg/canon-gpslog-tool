package Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Geerard
 */
public class DateUtil {

    private static final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    public static String format(Calendar time) {
        return format.format(time.getTime());
    }

    public static Calendar parse(String date) {
        System.out.println("parsing " + date);
        try {
            Calendar c = Calendar.getInstance();
            c.setTime(format.parse(date));
            return c;
        } catch (ParseException ex) {
            Logger.getLogger(DateUtil.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
