package Util;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.scene.image.Image;
import model.Entry;
import model.Route;

/**
 *
 * @author Geerard
 */
public class MapUtil {

    private Route route;
    private final static String urlRoot = "http://maps.googleapis.com/maps/api/staticmap?sensor=false&size=640x640&path=weight:3|color:red|enc:";

    public MapUtil(Route route) {
        this.route = route;
    }

    public Image getMap() {
        String url = urlRoot;
        ArrayList<LatLon> latlons = new ArrayList();
        for (Entry entry : route.getActiveEntries()) {
            latlons.add(new LatLon(entry.getLatitudeC(), entry.getLongitudeC()));
            // url += "|" + entry.getLatitudeC() + "," + entry.getLongitudeC();
        }
        url += createPolyLine(latlons);
        System.out.println(url);
        Image image = new Image(url);
        return image;
    }

    private String createPolyLine(ArrayList<LatLon> hp) {
        double oldlat = 0;
        double oldlon = 0;
        StringBuilder nb = new StringBuilder();
        for (int i = 0; i < hp.size(); i++) {
            LatLon temp = hp.get(i);

            double p1 = temp.getLat();
            double p2 = temp.getLon();

            if (Math.abs(p1 - oldlat) >= 0.00001) {
                String temp2 = encodePolyline(p1 - oldlat);
                nb.append(temp2);
            }
            if (Math.abs(p2 - oldlon) >= 0.00001) {
                String temp2 = encodePolyline(p2 - oldlon);
                nb.append(temp2);
            }
            oldlat = p1;
            oldlon = p2;
        }

        String temp = nb.toString();

        //Check temp for "*\*" pattern
        Pattern pattern = Pattern.compile("\".*\\.*\"");
        Matcher matcher = pattern.matcher(temp);

        while (matcher.find()) {
            //Use matcher.start() and .end() to replace "\" with "\\"
            temp = temp.substring(0, matcher.start()) + temp.substring(matcher.start(), matcher.end()).replaceAll("\\\\", "\\\\\\\\") + temp.substring(matcher.end());
        }

        return temp;
    }

    private String encodePolyline(double latlong) {
        //Google's procedure for encoding polyline data
        //This doesn't cater for backslashes in string literals i.e. the character sequence "\"
        //which should be returned as "\\". Function createPolyLine will do this.

        String poly;
        int signNum = (int) Math.signum(latlong);

        if (signNum < 0) {
            poly = Long.toBinaryString((Math.round(Math.abs(latlong) * 1e5) - 1)) + 1;
        } else {
            poly = Long.toBinaryString(2 * (Math.round(Math.abs(latlong) * 1e5)));
        }

        //Change nc to start from first 1
        //String nc2 = nc.substring(nc.indexOf("1"));
        String nc2 = poly.substring(poly.indexOf("1"));

        //Put the 5 bit chunks in reverse order
        StringBuilder nc3 = new StringBuilder();
        int i = nc2.length();
        while (i > 5) {
            //OR each value with 0x20 if ano
            nc3.append("1");
            nc3.append(nc2.substring(i - 5, i));
            i = i - 5;
        }
        nc3.append("0");

        for (int j = 1; j < 6 - i; j++) {
            nc3.append("0");
        }
        nc3.append(nc2.substring(0, i));

        //Convert each 6 bits to decimal
        StringBuilder nc4 = new StringBuilder();
        for (int j = 0; j < nc3.length(); j = j + 6) {
            char c = (char) (Integer.parseInt(nc3.substring(j, j + 6), 2) + 63);
            nc4.append(c);
        }
        poly = nc4.toString();

        return poly;
    }
}
