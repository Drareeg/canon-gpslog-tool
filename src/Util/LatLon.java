package Util;

/**
 *
 * @author Geerard
 */
class LatLon {

    private double lat;
    private double lon;

    public LatLon(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    double getLat() {
        return lat;
    }

    double getLon() {
        return lon;
    }
}
