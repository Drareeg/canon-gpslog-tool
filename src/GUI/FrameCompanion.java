/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Util.DateUtil;
import Util.MapUtil;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import model.Entry;
import model.Route;

/**
 *
 * @author Geerard
 */
public class FrameCompanion implements Initializable {

    //private GPS gps;
    @FXML
    private AnchorPane frame;
    @FXML
    private TableView<Entry> dataTable;
    @FXML
    private LineChart<Number, Number> speedChart;
    @FXML
    private Label distanceLabel;
    @FXML
    private Label speedLabel;
    @FXML
    private ComboBox fromComboBox;
    @FXML
    private ComboBox toComboBox;
    @FXML
    private ImageView mapView;
    private Route route;
    private MapUtil mapUtil;

    public FrameCompanion() {
    }

    @FXML
    private void openFile(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        //fileChooser.setInitialDirectory(new File("C:\\Users\\Geerard\\Dropbox\\GPS"));
        fileChooser.setTitle("Select logfile");
        File chosenFile = fileChooser.showOpenDialog(null);
        try {
            route = new Route(chosenFile);
            mapUtil = new MapUtil(route);
            update();
            updateComboBoxes();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FrameCompanion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void update() {
        updateTable();
        updateChart();
        updateMap();
    }

    public void updateTable() {
        ObservableList<Entry> entries = route.getActiveEntries();
        dataTable.setItems(entries);
    }

    public void updateChart() {
        System.out.println("updating chart");
        XYChart.Series series = new XYChart.Series();
        System.out.println("found " + route.getActiveEntries().size() + " active entries");
        for (Entry e : route.getActiveEntries()) {
            series.getData().add(new XYChart.Data(e.getTimeC(), e.getSpeedC()));
        }
        speedChart.getData().clear();
        speedChart.getData().add(series);
        speedChart.setPrefWidth(speedChart.getData().size() * 15.0);
    }

    public void updateMap() {
        mapView.setImage(mapUtil.getMap());
        mapView.setFitHeight(0.0);
        mapView.setFitWidth(0.0);
    }

    public void updateDistanceLabelAndSpeedLabel() {
        float totalDistInMeters = route.getTotalActiveDistance();
        distanceLabel.setText(String.format("%.2f", totalDistInMeters / 1000) + " km");
        String start = (String) fromComboBox.getSelectionModel().getSelectedItem();
        String end = (String) toComboBox.getSelectionModel().getSelectedItem();
        Calendar startC = DateUtil.parse(start);
        Calendar endC = DateUtil.parse(end);
        long diffinMilis = endC.getTimeInMillis() - startC.getTimeInMillis();
        if (diffinMilis <= 0) {
            speedLabel.setText("0.0 km/h");
        } else {
            float diffInHours = (diffinMilis + 0.0f) / 1000 / 60 / 60;
            float avgSpeed = (totalDistInMeters / 1000) / diffInHours;
            speedLabel.setText(String.format("%.2f", avgSpeed) + "km/h");
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth();
        double height = screenSize.getHeight();
        frame.setPrefHeight(height - 50);
        frame.setPrefWidth(width - 5);
        frame.setLayoutX(0.0);
        frame.setLayoutY(0.0);
    }

    private void updateComboBoxes() {
        fromComboBox.getItems().clear();
        fromComboBox.getItems().addAll(route.getallDates());
        toComboBox.getItems().clear();
        toComboBox.getItems().addAll(route.getallDates());
        fromComboBox.getParent().setVisible(true);
    }

    public void onIntervalAdd() {
        int from = fromComboBox.getSelectionModel().getSelectedIndex();
        int to = toComboBox.getSelectionModel().getSelectedIndex();
        ObservableList<Entry> entries = route.getEntries();
        for (Entry e : entries) {
            e.setActive(false);
        }
        for (int i = from; i <= to; i++) {
            entries.get(i).setActive(true);
        }
        update();
        updateDistanceLabelAndSpeedLabel();
    }
}
