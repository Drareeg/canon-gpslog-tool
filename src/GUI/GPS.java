/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Geerard
 */
public class GPS extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Frame.fxml"));

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.show();
    }

//netbeans negeert main(), instellen in properties
    public static void main(String[] args) {
        launch(args);
    }
}
